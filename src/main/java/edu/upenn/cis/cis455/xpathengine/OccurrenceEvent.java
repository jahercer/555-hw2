package edu.upenn.cis.cis455.xpathengine;

/**
 This class encapsulates the tokens we care about parsing in XML (or HTML)
 */
public class OccurrenceEvent {
	public enum Type {Open, Close, Text};
	
	Type type;
	String value;
	int depth;
	
	public OccurrenceEvent(Type t, String value, int d) {
		this.type = t;
		this.value = value;
		this.depth = d;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public int getDepth() {
		return depth;
	}

	public void setDepth(int d) {
		this.depth = d;
	}

	public String toString() {
		if (type == Type.Open) 
			return "<" + value + ">";
		else if (type == Type.Close)
			return "</" + value + ">";
		else
			return value;
	}
}
