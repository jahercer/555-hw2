package edu.upenn.cis.cis455.xpathengine;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class XPathEngineClass implements XPathEngine {

	String[] paths;
	Map<String, Integer> pathPos = new HashMap<String, Integer>();
	private StorageInterface db;
	private int max = 0;
	
	public XPathEngineClass() {
		this.db = Crawler.getDb();
		this.paths = db.getPaths();
	}
	
	@Override
	public void setXPaths(String[] expressions) {
		this.paths = expressions;
	}
	
	public void setPathPos(Map<String, Integer> pathPos) {
		this.pathPos = pathPos;
	}

	@Override
	public boolean[] evaluateEvent(OccurrenceEvent event) {
		boolean[] res = new boolean[paths.length];
		
		for (int i = 0; i < res.length; i++) {
			String path = paths[i];
			LinkedList<String> list = createList(path);
			int state = pathPos.get(path);
			String node = list.get(state);
			System.out.println("TARGET: " +node);
			
			if (event.getType().equals(OccurrenceEvent.Type.Open) || event.getType().equals(OccurrenceEvent.Type.Close)) {
				if (event.getValue().equals(node)) {
					System.out.println(event.getValue() + " == " + node +"--> true");
					res[i] = true;
				} else {
					res[i] = false;
				}
			} else { //Text event
				if (node.startsWith("[")) {
					String[] words = node.split("\"");
					String word = words[words.length-2];
					if (event.getValue().equals(word)) {
						res[i] = true;
					} else {
						res[i] = false;
					}
				} else {
					res[i] = false;
				}
			}
		}
		
		return res;
	}
	
	private LinkedList<String> createList(String path) {
        String[] pathArr = path.split("/");
        LinkedList<String> linkedList = new LinkedList<String>();
        
        for (int i = 1; i < pathArr.length; i++) {
        	if (pathArr[i].contains("[")) {
        		int e = pathArr[i].indexOf('[');
        		linkedList.add(pathArr[i].substring(0, e));
        		linkedList.add(pathArr[i].substring(e));
        	} else {
        		linkedList.add(pathArr[i]);
        	}
        }
        max = linkedList.size();
        return linkedList;
	}

	public String[] getXPaths() {
		return paths;
	}
	
	public int getMax() {
		return max;
	}
	
	public int getState() {
		return 0;
	}

}
