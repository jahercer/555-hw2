package edu.upenn.cis.cis455.crawler;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.RequestUtil;
import edu.upenn.cis.cis455.crawler.utils.Response;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.classes.Document;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DocumentBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(DocumentBolt.class);
	
	Fields schema = new Fields("url", "document");
  
    String executorId = UUID.randomUUID().toString();

    private OutputCollector collector;
	
    public DocumentBolt() {
    	log.debug("Document Bolt started");
    }
    
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);

	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute(Tuple input) {
		String url = input.getStringByField("url");
		URLInfo urlInfo = new URLInfo(url);
		
		Response response = null;
		
		if (!urlInfo.isSecure()) { //HTTP
			try {
				response = RequestUtil.nonSecureRequest("GET", url, null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else { //HTTPS
			try {
				response = RequestUtil.secureRequest("GET", url, null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String contentType = response.getHeaders().get("content-type").split(";")[0];
		Document doc = new Document(response.getBody(), contentType);
		
		System.out.println("Document Bolt emitting!");
		this.collector.emit(new Values<Object>(url, doc));

	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);

	}

	@Override
	public Fields getSchema() {
		return schema;
	}

}
