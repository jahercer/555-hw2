package edu.upenn.cis.cis455.crawler.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;

public class ResponseParser {

	public static Response getResponse(HttpURLConnection con) throws IOException {
		Response res = new Response(con.getResponseCode());
		//System.out.println(con.getResponseCode());
        //Get Headers
		con.getHeaderFields().entrySet().stream()
		  .filter(entry -> entry.getKey() != null)
		  .forEach(entry -> {
		      List<String> headerValues = entry.getValue();
		      Iterator<String> it = headerValues.iterator();
		      String values = "";
		      if (it.hasNext()) {
		          values += it.next();
		          while (it.hasNext()) {
		              values += "; " + it.next();
		          }
		      }
		      res.getHeaders().put(entry.getKey().toLowerCase(), values);
		});

        // Get Body
		BufferedReader in = null;
		if (con.getResponseCode() >= 200 && con.getResponseCode() < 300) {
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine+"\n");
			}
			in.close();
			res.setBody(content.toString());
		}
		
		con.disconnect();

        return res;
    }
}
