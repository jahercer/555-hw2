package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.classes.Password;

public class LoginHandler implements Route {
    StorageInterface db;

    public LoginHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException, NoSuchAlgorithmException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        System.err.println("Login request for " + user + " and " + pass);
        
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
    	byte[] encodedhash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
    	Password password = new Password(encodedhash);

        
        if (db.getSessionForUser(user, password)) {
            System.err.println("Logged in!");
            Session session = req.session();
            session.attribute("user", user);
            session.attribute("password", pass);
            session.maxInactiveInterval(5*60);
            resp.redirect("/.");
        } else {
            System.err.println("Invalid credentials");
            resp.redirect("/login-form.html");
        }

        return "";
    }
}
