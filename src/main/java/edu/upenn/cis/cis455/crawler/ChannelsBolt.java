package edu.upenn.cis.cis455.crawler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.classes.Document;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngineClass;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class ChannelsBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(ChannelsBolt.class);
	
	Fields schema = new Fields();
  
    String executorId = UUID.randomUUID().toString();

    private OutputCollector collector;
    
    private StorageInterface db;
    
    String[] paths;
    Map<Document, Map<String, Integer>> docs = new HashMap<Document, Map<String, Integer>>(); //State within each path by Document
	Map<Document, List<String>> elements = new HashMap<Document, List<String>>(); //Open elements by Document
	
    public ChannelsBolt() {
    	this.db = Crawler.getDb();
    	log.debug("Channels Bolt started");
    }
	
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);

	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute(Tuple input) {
		//System.out.println("Channels bolt receiving!");
		OccurrenceEvent event = (OccurrenceEvent) input.getObjectByField("event");
		Document doc = (Document) input.getObjectByField("document");
		String url = input.getStringByField("url");
		
		Map<String, Integer> pathPos = new HashMap<String, Integer>(); //State within each path
		List<String> elem = new ArrayList<String>();
		
		XPathEngineClass eng = (XPathEngineClass) XPathEngineFactory.getXPathEngine();
		//eng.setXPaths(paths);
		paths = eng.getXPaths();
		
		//Initializing Maps
		boolean found = false;
		for (Document d : docs.keySet()) {
			if (Arrays.equals(d.getHashedContent(), doc.getHashedContent())) {
				found = true;
				break;
			}
		}
		if (found) {
			//System.out.println("Doc found!");
			pathPos = docs.get(doc);
			elem = elements.get(doc);
		} else {
			//System.out.println("Doc NOT found!");
			initializeMap(pathPos);
			docs.put(doc, pathPos);
			elements.put(doc, elem);
		}
		
		eng.setPathPos(pathPos);
		
		
		boolean[] res = eng.evaluateEvent(event);
		
		
		for (int i = 0; i < res.length; i++) {
			String path = paths[i];
			int state = pathPos.get(path);
			if (res[i]) {
				if (event.getType().equals(OccurrenceEvent.Type.Open)) {
					System.out.println(eng.getMax());
					if (state == eng.getMax()) {
						db.addMatchedUrl(path, url);
						System.out.println("DOCUMENT MATCHED");
					} else {
						pathPos.put(path, state++); //We go to next node
						eng.setPathPos(pathPos);
						elem.add(event.getValue()); //Add open element!
						elements.put(doc, elem);
					}
				} else if (event.getType().equals(OccurrenceEvent.Type.Close)) {
					if (state == 0) {
						//We close document!!
						//Delete from Maps
						docs.remove(doc);
						elements.remove(doc);
					} else {
						pathPos.put(path, state--); //We go to previous node/state
						eng.setPathPos(pathPos);
						elem.remove(elem.lastIndexOf(event.getValue())); //Remove closed element!
						elements.put(doc, elem);
					}
				} else {
					//Text Event
					if (state == eng.getMax()) {
						db.addMatchedUrl(path, url);
					} else {
						pathPos.put(path, state++);
						eng.setPathPos(pathPos);
					}
				}
			} else if (event.getType().equals(OccurrenceEvent.Type.Close) && event.getDepth()==0) { //event.getLevel()==0
				if (state == 0) {
					//We close document!!
					//Delete from Maps
					docs.remove(doc);
					elements.remove(doc);
				}	
			}
		}
	}

	private void initializeMap(Map<String, Integer> pathPos) {
		for (int i = 0; i < paths.length; i++) {
			pathPos.put(paths[i], 0);
		}
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);

	}

	@Override
	public Fields getSchema() {
		return schema;
	}

}
