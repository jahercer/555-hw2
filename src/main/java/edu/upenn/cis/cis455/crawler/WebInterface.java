package edu.upenn.cis.cis455.crawler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.sleepycat.je.DatabaseException;

import static spark.Spark.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Level;

import edu.upenn.cis.cis455.crawler.handlers.AddSessionCookie;
import edu.upenn.cis.cis455.crawler.handlers.CreateChannelHandler;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.LogoutHandler;
import edu.upenn.cis.cis455.crawler.handlers.LookupHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegistrationHandler;
import edu.upenn.cis.cis455.crawler.handlers.ShowChannelHandler;
import edu.upenn.cis.cis455.crawler.handlers.WelcomeHandler;

public class WebInterface {
	final static Logger logger = LogManager.getLogger(WebInterface.class);
	
    public static void main(String args[]) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    	
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        port(45555);
        StorageInterface database = null;
		try {
			database = StorageFactory.getDatabaseInstance(args[0]);
		} catch (DatabaseException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }


        before("/*", "*/*", testIfLoggedIn);
        // TODO:  add /register, /logout, /index.html, /, /lookup
        post("/register", new RegistrationHandler(database));
        post("/login", new LoginHandler(database));
        get("/", new WelcomeHandler(database));
        get("/logout", new LogoutHandler());
        get("/lookup", new LookupHandler(database));
        get("/create/:name", new CreateChannelHandler(database));
        get("/show", new ShowChannelHandler(database));
        
        after("/*", "*/*", new AddSessionCookie(database));

        awaitInitialization();
    }
}
