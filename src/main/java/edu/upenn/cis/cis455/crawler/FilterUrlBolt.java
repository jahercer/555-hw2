package edu.upenn.cis.cis455.crawler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Queue;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.RequestUtil;
import edu.upenn.cis.cis455.crawler.utils.Response;
import edu.upenn.cis.cis455.crawler.utils.RobotsParser;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.classes.DisallowValue;
import edu.upenn.cis.cis455.storage.classes.WebPage;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import spark.Spark;

public class FilterUrlBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(FilterUrlBolt.class);
	
	Fields schema = new Fields();
  
    String executorId = UUID.randomUUID().toString();

    private OutputCollector collector;
    
    private Crawler crawler;
    private StorageInterface db;
    private Queue<String> urls = new LinkedList<String>();
	
	
	public FilterUrlBolt() {
		this.db = Crawler.getDb();
		this.urls = Crawler.getUrls();
		log.debug("Url Filter Bolt started"); 
	}
	 
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);

	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute(Tuple input) {
		System.out.println("Filter Received!");
		String url = input.getStringByField("url");
		URLInfo urlInfo = new URLInfo(url);
		
		//CHECK DISALLOWDB && ROBOTS.TXT
		DisallowValue dis = db.getDisallowInfo(urlInfo);
		if (dis == null) {
			//GET url/robots.txt
			Response response = null;
			if (!urlInfo.isSecure()) { //HTTP
				try {
					response = RequestUtil.nonSecureRequest("GET", urlInfo.toStringNoPath()+"/robots.txt", null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else { //HTTPS
				try {
					response = RequestUtil.secureRequest("GET", urlInfo.toStringNoPath()+"/robots.txt", null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (response != null && response.getStatusCode() == 200) {
				dis = RobotsParser.parseRobots(response.getBody());
			} else {
				dis = new DisallowValue();
			}
			db.addDisallowInfo(urlInfo.getHostName(), dis);
		}
		
		boolean allowed = true;
		if (dis.getPaths().contains("/")) { return; } //Next url
		else { //Check exact path
			String[] urlPath = urlInfo.getFilePath().split("/");
			List<String> paths = dis.getPaths();
			//paths.forEach(k -> System.out.println(k));
			ListIterator<String> it = paths.listIterator();
			while (it.hasNext()) {
				String[] disPath = it.next().split("/");
				for (int i = 1; i < disPath.length && i < urlPath.length; i++) {
					if (urlPath.length > 0 && disPath[i].equals(urlPath[i])) {
						if (i == disPath.length - 1) { // last component (and everything has matched)
							allowed = false;
						} 
					} else {
						break;
					}
				}
				if (!allowed) break;
			}
		}
		
		if (!allowed) {
			Spark.halt(404); //not allowed - next url
			return;
		}
		if (dis.getDelay() > 0) {
			Date lastAcc = db.getLastTimeRootUrl(urlInfo.getHostName());
			if (lastAcc.toInstant().plusSeconds(dis.getDelay()).isAfter(new Date().toInstant())) {
				//We can't access yet
				synchronized(urls) {
					urls.add(url); //We put back the url and check later
					urls.notifyAll();
				}

			}
		}
		
		//WE ARE ALLOWED TO PROCEED		
		String dateString = null;
		if (!db.checkContentSeen(url)) {
			if (db.checkURL(url)) {
				WebPage webPage = db.getURL(url);
				Date date = webPage.getDate();
				DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");  
				dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
				dateString = dateFormat.format(date);
			}
		} else {
			return;
		}
		
		Response response = null;
		if (!urlInfo.isSecure()) { //HTTP
			try {
				response = RequestUtil.nonSecureRequest("HEAD", url, dateString);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else { //HTTPS
			try {
				response = RequestUtil.secureRequest("HEAD", url, dateString);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		String contentType = null;
		
		if (response.getStatusCode() == 304)  {
			log.info(url+": not modified");
			return;
		} else if (response.getStatusCode() == 200){
			contentType = response.getHeaders().get("content-type").split(";")[0];
			if (contentType.equals("text/html") || contentType.equals("text/xml") 
					|| contentType.equals("application/xml") || contentType.contains("+xml")) {
				if (Integer.valueOf(response.getHeaders().get("content-length"))/1048576.0 <= Crawler.getSize()) {
					synchronized(urls) {
						System.out.println("Now adding: "+url);
						urls.add(url); //We put back the url and check later
						urls.notifyAll();
					}					
				} else {
					Spark.halt(404);
					return;
				}
			} else {
				Spark.halt(404);
				return;
			}
		} else {
			log.error(url+": error");
			return;
		}

	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);

	}

	@Override
	public Fields getSchema() {
		return schema;
	}
	

}
