package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.HaltException;

public class WelcomeHandler implements Route {

	private StorageInterface db;
	
	public WelcomeHandler(StorageInterface db) {
		this.db = db;
	}
	
    @Override
    public String handle(Request req, Response resp) throws HaltException {
    	String res = "<!DOCTYPE html>\n"
    			+ "<html>\n"
    			+ "<head>\n"
    			+ "    <title>\"Welcome\"</title>\n"
    			+ "</head>\n"
    			+ "<body>\n"
    			+ "<header>\n"
    			+ "		<h1>Welcome "+req.session().attribute("user")+"</h1>\n"
    			+ "</header>\n"
    			+ "<ul>\n";
    	
    	for (String s : db.getChannels()) {
    		res += "<li><a href=\"/show?channel="+s+"\">"+s+"</a></li>\n";
    	}
    	
    	res += "</ul>\n</body>\n</html>";
    	
    	
    	return res;
    }
}
