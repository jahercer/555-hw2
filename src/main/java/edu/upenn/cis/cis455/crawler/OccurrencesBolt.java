package edu.upenn.cis.cis455.crawler;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.NodeVisitor;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class OccurrencesBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(OccurrencesBolt.class);
	
	Fields schema = new Fields("event", "url", "document");
  
    String executorId = UUID.randomUUID().toString();

    private OutputCollector collector;
	
    public OccurrencesBolt() {
    	log.debug("Occurrences Bolt started");
    }
	
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);

	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute(Tuple input) {
		System.out.println("Occurrences Bolt receiving!");
		String url = input.getStringByField("url");
		edu.upenn.cis.cis455.storage.classes.Document document = (edu.upenn.cis.cis455.storage.classes.Document) input.getObjectByField("document");
		
		org.jsoup.nodes.Document doc = Jsoup.parse(document.getRawContent());

		Node root = doc.root();
		
		root.traverse(new NodeVisitor() {
			public void head(Node node, int depth) {
				if (node instanceof Element) {
					collector.emit(new Values<Object>(new OccurrenceEvent(
							OccurrenceEvent.Type.Open,
							((Element) node).tagName(),
							depth), url, document));
					if (((Element) node).hasText()) {
						collector.emit(new Values<Object>(new OccurrenceEvent(
								OccurrenceEvent.Type.Text,
								((Element) node).text(),
								depth), url, document));
					}
				}
		    }
		    public void tail(Node node, int depth) {
		    	if (node instanceof Element) {
			    	if (!((Element) node).hasText()) {
						collector.emit(new Values<Object>(new OccurrenceEvent(
								OccurrenceEvent.Type.Close,
								((Element) node).tagName(),
								depth), url, document));
					}
		    	}
		    }
		});
		
	}


	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;

	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);

	}

	@Override
	public Fields getSchema() {
		return schema;
	}

}
