package edu.upenn.cis.cis455.crawler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Level;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sleepycat.je.DatabaseException;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.tuple.Fields;

public class Crawler implements CrawlMaster {
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.
	final static Logger logger = LogManager.getLogger(Crawler.class);

    static final int NUM_WORKERS = 10;
    
    private static final String URL_SPOUT = "URL_SPOUT";
    private static final String DOCUMENT_BOLT = "DOCUMENT_BOLT";
    private static final String EXTRACT_LINKS_BOLT = "EXTRACT_LINKS_BOLT";
    private static final String FILTER_URL_BOLT = "FILTER_URL_BOLT";
    private static final String OCCURRENCES_BOLT = "OCCURRENCES_BOLT";
    private static final String CHANNELS_BOLT = "CHANNELS_BOLT";

    protected String startUrl;
    protected static StorageInterface db;
    protected static int size;
    protected int count = 0;
    protected int max_count;
    
    protected static Queue<String> urls = new LinkedList<String>();
    
    private List<Long> workers = new ArrayList<Long>();
    

    public Crawler(String startUrl, StorageInterface db, int size, int max_count) {
    	this.startUrl = startUrl;
		this.db = db;
		this.size = size;
		this.max_count = max_count;
    }

    /**
     * Main thread
     */
    public void start() {
    	CrawlerWorker w = new CrawlerWorker(this, urls, db);
		Thread crawlerThread = new Thread(w);
		crawlerThread.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//Create crawler workers
		int i = 1;
    	while(i <= NUM_WORKERS && !isDone()) {
    		if (urls.isEmpty()) {
    			try {
    				synchronized(urls) {
    					urls.wait(500);
    				}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		} else {
	    		w = new CrawlerWorker(this, urls, db);
	    		crawlerThread = new Thread(w);
	    		crawlerThread.start();
	    		i++;
	    		try {
					Thread.currentThread().sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	} 
    	
    	
    }

    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
		count++;
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
        return this.urls.isEmpty() || count >= max_count;
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
    	if (working) {
    		synchronized (workers) {
    			this.workers.add(Thread.currentThread().getId());
    			workers.notifyAll();
    		}
    	}
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
    	synchronized (workers) {
    		this.workers.remove(Thread.currentThread().getId());
    		workers.notifyAll();
    	}
    }
    
    private List<Long> getWorkers() {
		return this.workers;
	}
    
    public static Queue<String> getUrls() {
		return urls;
	}
    
    public static StorageInterface getDb() {
		return db;
	}
    
    public static int getSize() {
		return size;
	}

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    	
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        if (!Files.exists(Paths.get(envPath))) {
            try {
                Files.createDirectory(Paths.get(envPath));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        StorageInterface db = null;
		try {
			db = StorageFactory.getDatabaseInstance(envPath);
		} catch (DatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        Crawler crawler = new Crawler(startUrl, db, size, count);
        crawler.urls.add(startUrl);
        
        
        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        //crawler.start();

		/*
		 * while (!crawler.isDone()) try { Thread.sleep(10); } catch
		 * (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * while (!crawler.getWorkers().isEmpty()) { try { Thread.sleep(10); } catch
		 * (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } }
		 */
        
        Config config = new Config();

        UrlSpout urlSpout = new UrlSpout();
        DocumentBolt documentBolt = new DocumentBolt();
        ExtractLinksBolt extractLinkBolt = new ExtractLinksBolt();
        FilterUrlBolt filterUrlBolt = new FilterUrlBolt();
        OccurrencesBolt occurrencesBolt = new OccurrencesBolt();
        ChannelsBolt channelsBolt = new ChannelsBolt();

        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the url queue
        builder.setSpout(URL_SPOUT, urlSpout, 1);
        
        // One Bolt for emitting Documents
        builder.setBolt(DOCUMENT_BOLT, documentBolt, 1).shuffleGrouping(URL_SPOUT);

        // Lower Branch
        builder.setBolt(EXTRACT_LINKS_BOLT, extractLinkBolt, 4).shuffleGrouping(DOCUMENT_BOLT);
        builder.setBolt(FILTER_URL_BOLT, filterUrlBolt, 4).shuffleGrouping(EXTRACT_LINKS_BOLT);
        
        // Upper Branch
        builder.setBolt(OCCURRENCES_BOLT, occurrencesBolt, 4).shuffleGrouping(DOCUMENT_BOLT);
        builder.setBolt(CHANNELS_BOLT, channelsBolt, 4).fieldsGrouping(OCCURRENCES_BOLT, new Fields("document"));

        LocalCluster cluster = new LocalCluster();
        
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
		try {
			String str = mapper.writeValueAsString(topo);

			System.out.println("The StormLite topology is:\n" + str);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
        
        
        cluster.submitTopology("crawler", config, 
        		builder.createTopology());
        try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        cluster.killTopology("crawler");
        cluster.shutdown();
        
        db.close();
        System.out.println("Done crawling!");
        System.exit(0);
    }

}
