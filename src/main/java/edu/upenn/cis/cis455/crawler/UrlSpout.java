package edu.upenn.cis.cis455.crawler;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.spout.IRichSpout;
import edu.upenn.cis.stormlite.spout.SpoutOutputCollector;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;

public class UrlSpout implements IRichSpout {

	static Logger log = LogManager.getLogger(UrlSpout.class);

    String executorId = UUID.randomUUID().toString();

	SpoutOutputCollector collector;
	
	Queue<String> urls = new LinkedList<String>();
	
	public UrlSpout() {
		this.urls = Crawler.getUrls();
		log.debug("URL Spout Started");
	}
	
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("url"));

	}

	@Override
	public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
		this.collector = collector;

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	@Override
	public void nextTuple() {
		String url = "";
		while (true) {
			synchronized(urls) { 
				url = urls.poll(); //Extract next url
				urls.notifyAll();
				if (url == null) {
					try {
						System.out.println("Url spout Waiting");
						urls.wait(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					break;
				}
			}
		}
		System.out.println("Url Spout Emitting!");
		this.collector.emit(new Values<Object>(url));

	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);

	}

}
