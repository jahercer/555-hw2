package edu.upenn.cis.cis455.crawler.utils;

import edu.upenn.cis.cis455.storage.classes.DisallowValue;

public class RobotsParser {

	public static DisallowValue parseRobots(String robotsTxt) {
		DisallowValue dis = new DisallowValue();
		String[] robots = robotsTxt.split("User-agent: ");
		
		for (int i = 0; i < robots.length; i++) {
			String name = robots[i].lines().findFirst().get();
			robots[i].lines().skip(1);
			if (name.equals("*") && !robotsTxt.contains("cis455crawler")|| name.equals("cis455crawler")) {
				robots[i].lines().forEach(line -> {
					String[] lineArr = line.split(": ", 2);
					if (lineArr[0].equals("Disallow")) {
						dis.getPaths().add(lineArr[1]);
					} else if (lineArr[0].equals("Crawl-delay")) {
						dis.setDelay(Integer.valueOf(lineArr[1]));
					}
				});
			}
		}
		
		return dis;
    }
}
