package edu.upenn.cis.cis455.crawler.utils;

import java.util.HashMap;
import java.util.Map;

public class Response {

	private int statusCode;
	private Map<String, String> headers;
	private String body;
	
	public Response(int statusCode) {
		this.setStatusCode(statusCode);
		this.headers = new HashMap<String, String>();
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	
}
