package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Spark;
import spark.Response;
import spark.HaltException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.classes.Password;

public class RegistrationHandler implements Route {
    StorageInterface db;

    public RegistrationHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException, NoSuchAlgorithmException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        System.err.println("Registration request for " + user);
        if (db.checkUser(user)) {
        	Spark.halt(404);
        	return "";
        } else {
        	MessageDigest digest = MessageDigest.getInstance("SHA-256");
        	byte[] encodedhash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
        	Password password = new Password(encodedhash);
        	
        	db.addUser(user, password);
        	resp.status(200);
        	return "<!DOCTYPE html>\n"
        			+ "<html>\n"
        			+ "<head>\n"
        			+ "    <title>Register success</title>\n"
        			+ "</head>\n"
        			+ "<body>\n"
        			+ "<h1>Account created</h1>\n"
        			+ "\n"
        			+ "<a href=\"/login-form.html\">Press to login</a>\n"
        			+ "\n"
        			+ "</body>\n"
        			+ "</html>";
        }
    }
}

