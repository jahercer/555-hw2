package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Spark;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.classes.Document;

public class LookupHandler implements Route {

	 StorageInterface db;

	    public LookupHandler(StorageInterface db) {
	        this.db = db;
	    }

	    @Override
	    public String handle(Request req, Response resp) throws HaltException {
	        Document doc = db.getDocument(req.queryParams("url"));
	        if (doc == null) Spark.halt(404);
	        
	        resp.header("Content-Type", doc.getContentType());

	        return doc.getRawContent();
	    }
	
}
