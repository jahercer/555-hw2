package edu.upenn.cis.cis455.crawler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Queue;
import java.util.TimeZone;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.utils.Response;
import edu.upenn.cis.cis455.crawler.utils.ResponseParser;
import edu.upenn.cis.cis455.crawler.utils.RobotsParser;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.classes.DisallowValue;
import edu.upenn.cis.cis455.storage.classes.Document;
import edu.upenn.cis.cis455.storage.classes.WebPage;

public class CrawlerWorker implements Runnable {
	final static Logger logger = LogManager.getLogger(CrawlerWorker.class);
	
	private Crawler crawler;
	private Queue<String> urls = new LinkedList<String>();
	private StorageInterface db;
	
	public CrawlerWorker(Crawler crawler, Queue<String> urls, StorageInterface db) {
		this.crawler = crawler;
		this.urls = urls;
		this.db = db;
	}

	@Override
	public void run() {
		crawler.setWorking(true);
		while (!crawler.isDone()) {
			String url = "";
			synchronized(urls) { 
				url = urls.poll(); //Extract next url
				urls.notifyAll();
			}
			
			URLInfo urlInfo = new URLInfo(url);
			
			//CHECK DISALLOWDB && ROBOTS.TXT
			DisallowValue dis = db.getDisallowInfo(urlInfo);
			if (dis == null) {
				//GET url/robots.txt
				Response response = null;
				if (!urlInfo.isSecure()) { //HTTP
					try {
						response = nonSecureRequest("GET", urlInfo.toStringNoPath()+"/robots.txt", null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else { //HTTPS
					try {
						response = secureRequest("GET", urlInfo.toStringNoPath()+"/robots.txt", null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (response != null && response.getStatusCode() == 200) {
					dis = RobotsParser.parseRobots(response.getBody());
				} else {
					dis = new DisallowValue();
				}
				db.addDisallowInfo(urlInfo.getHostName(), dis);
			}
			
			boolean allowed = true;
			if (dis.getPaths().contains("/")) { continue; } //Next url
			else { //Check exact path
				String[] urlPath = urlInfo.getFilePath().split("/");
				List<String> paths = dis.getPaths();
				//paths.forEach(k -> System.out.println(k));
				ListIterator<String> it = paths.listIterator();
				while (it.hasNext()) {
					String[] disPath = it.next().split("/");
					for (int i = 1; i < disPath.length && i < urlPath.length; i++) {
						if (urlPath.length > 0 && disPath[i].equals(urlPath[i])) {
							if (i == disPath.length - 1) { // last component (and everything has matched)
								allowed = false;
							} 
						} else {
							break;
						}
					}
					if (!allowed) break;
				}
			}
			
			if (!allowed) continue; //not allowed - next url
			if (dis.getDelay() > 0) {
				Date lastAcc = db.getLastTimeRootUrl(urlInfo.getHostName());
				if (lastAcc.toInstant().plusSeconds(dis.getDelay()).isAfter(new Date().toInstant())) {
					//We can't access yet
					synchronized(urls) {
						urls.add(url); //We put back the url and check later
						urls.notifyAll();
					}

				}
			}
			
			//WE ARE ALLOWED TO PROCEED		
			String dateString = null;
			if (!db.checkContentSeen(url)) {
				if (db.checkURL(url)) {
					WebPage webPage = db.getURL(url);
					Date date = webPage.getDate();
					DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");  
					dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
					dateString = dateFormat.format(date);
				}
			} else {
				continue;
			}
			
			Response response = null;
			if (!urlInfo.isSecure()) { //HTTP
				try {
					response = nonSecureRequest("HEAD", url, dateString);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else { //HTTPS
				try {
					response = secureRequest("HEAD", url, dateString);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			String contentType = null;
			
			if (response.getStatusCode() == 304)  {
				logger.info(url+": not modified");
				continue;
			} else if (response.getStatusCode() == 200){
				contentType = response.getHeaders().get("content-type").split(";")[0];
				if (contentType.equals("text/html") || contentType.equals("text/xml") 
						|| contentType.equals("application/xml") || contentType.contains("+xml")) {
					if (Integer.valueOf(response.getHeaders().get("content-length"))/1048576.0 <= crawler.getSize()) {
						logger.info(url+": downloading");
						if (!urlInfo.isSecure()) { //HTTP
							try {
								response = nonSecureRequest("GET", url, dateString);
							} catch (Exception e) {
								e.printStackTrace();
							}
						} else { //HTTPS
							try {
								response = secureRequest("GET", url, dateString);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} else continue;
				} else continue;
			} else {
				logger.error(url+": error");
				continue;
			}
			
			Document doc = new Document(response.getBody(), contentType);
			UUID docID = db.findHashedDocument(doc.getHashedContent());
			
			boolean hashed = false;
			
			if (docID != null) {
				db.addUrlOnly(url, docID);
				hashed = true;
			} else {
				docID = db.addDocument(url, doc);
				crawler.incCount();
			}
			
			db.addContentSeen(url, docID);
			
			if (contentType.equals("text/html") && !hashed) {
				org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(response.getBody());

				Elements links = jsoupDoc.select("a[href]");
				for (Element link : links) {
					String extractedUrl = link.attr("href");
					if (! extractedUrl.contains("http")) extractedUrl = url + extractedUrl;
					synchronized(urls) {
						urls.add(extractedUrl);
						urls.notifyAll();
					}
				}
			}
		}
			
		crawler.notifyThreadExited();
		return;
	}
	
	public Response nonSecureRequest(String method, String urlString, String date) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(method);
		con.setRequestProperty("User-Agent", "cis455crawler");
		if (date != null) con.setRequestProperty("If-Modified-Since", date);
		
		con.getResponseCode(); //Execute the request
		
		Response response = ResponseParser.getResponse(con);
			
		return response;
	}
	
	public Response secureRequest(String method, String urlString, String date) throws IOException {
		URL url = new URL(urlString);
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setRequestMethod(method);
		con.setRequestProperty("User-Agent", "cis455crawler");
		if (date != null) con.setRequestProperty("If-Modified-Since", date);
		
		con.getResponseCode(); //Execute the request
		
		Response response = ResponseParser.getResponse(con);
			
		return response;
	}
	

}
