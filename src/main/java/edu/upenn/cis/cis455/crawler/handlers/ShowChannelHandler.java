package edu.upenn.cis.cis455.crawler.handlers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.classes.Channel;
import edu.upenn.cis.cis455.storage.classes.Document;
import edu.upenn.cis.cis455.storage.classes.WebPage;
import spark.Request;
import spark.Response;
import spark.Route;

public class ShowChannelHandler implements Route {

	private StorageInterface db;
	
	public ShowChannelHandler(StorageInterface db) {
		this.db = db;
	}
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		String name = request.queryParams("channel");

		Channel channel = db.getChannel(name);
		
		String username = channel.getUsername();
		
		String res = "<!DOCTYPE html>\n"
    			+ "<html>\n"
    			+ "<head>\n"
    			+ "    <title>"+name+"</title>\n"
    			+ "</head>\n"
    			+ "<body>\n"
    			+ "<div class=”channelheader”>\n"
    			+ "<header>\n"
    			+ "		<h1>Channel name: "+name+", created by: "+username+"</h1>\n"
    			+ "</header>\n"
    			+ "</div>\n"
    			+ "<div class=”matches”>\n";
		
		List<String> urls = channel.getUrls();
		urls.forEach(k -> System.out.println(k));
		Iterator it = urls.iterator();
		while(it.hasNext()) {
			String url = (String) it.next();
			WebPage wp = db.getURL(url);
			Date date = wp.getDate();
			String d = new SimpleDateFormat("YYYY-MM-DD'T'hh:mm:ss", Locale.US).format(date);
			Document doc = db.getDocument(url);
			String content = doc.getRawContent();
			
			res += "<p>Crawled on: "+d+"</p>\n"
					+ "<p>Location: "+url+"</p>\n"
					+ "<div class=”document”>\n"
					+ "<p>"+content+"</p>\n"
					+ "</div>";
		}
		
		res += "</div>\n</body>\n</html>";
		
		return res;
	}

}
