package edu.upenn.cis.cis455.crawler.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class RequestUtil {

	public static Response nonSecureRequest(String method, String urlString, String date) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(method);
		con.setRequestProperty("User-Agent", "cis455crawler");
		if (date != null) con.setRequestProperty("If-Modified-Since", date);
		
		con.getResponseCode(); //Execute the request
		
		Response response = ResponseParser.getResponse(con);
			
		return response;
	}
	
	public static Response secureRequest(String method, String urlString, String date) throws IOException {
		URL url = new URL(urlString);
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setRequestMethod(method);
		con.setRequestProperty("User-Agent", "cis455crawler");
		if (date != null) con.setRequestProperty("If-Modified-Since", date);
		
		con.getResponseCode(); //Execute the request
		
		Response response = ResponseParser.getResponse(con);
			
		return response;
	}
	
}
