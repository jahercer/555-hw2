package edu.upenn.cis.cis455.crawler.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Filter;
import spark.Response;
import spark.Session;


public class AddSessionCookie implements Filter {
    Logger logger = LogManager.getLogger(LoginFilter.class);
    
    public AddSessionCookie(StorageInterface db) {}

    @Override
    public void handle(Request req, Response resp) throws Exception {
    	Session s = req.session(false);
        if (s != null) {
        	resp.cookie("JSESSIONID", s.id());
        }
    }
}
