package edu.upenn.cis.cis455.crawler;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.storage.classes.Document;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class ExtractLinksBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(ExtractLinksBolt.class);
	
	Fields schema = new Fields("url");
  
    String executorId = UUID.randomUUID().toString();

    private OutputCollector collector;
	
    public ExtractLinksBolt() {
    	log.debug("Link extractor Bolt started");
    }
	
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);

	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute(Tuple input) {
		String url = input.getStringByField("url");
		Document doc = (Document) input.getObjectByField("document");
		System.out.println("Document received!");
		
		if (doc.getContentType().equals("text/html")) {
			org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(doc.getRawContent());

			Elements links = jsoupDoc.select("a[href]");
			for (Element link : links) {
				String extractedUrl = link.attr("href");
				if (!extractedUrl.contains("http")) extractedUrl = url + extractedUrl;
				System.out.println("Extracting Links emitting!");
				this.collector.emit(new Values<Object>(extractedUrl));
			}
		}
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		return schema;
	}

}
