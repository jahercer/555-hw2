package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.classes.Channel;
import spark.Request;
import spark.Response;
import spark.Route;

public class CreateChannelHandler implements Route {

	private StorageInterface db;
	
	public CreateChannelHandler(StorageInterface db) {
		this.db = db;
	}
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		String name = request.params("name");
		String path = request.queryParams("xpath");
		
		String username = request.session().attribute("user");
		
		Channel channel = new Channel(username, path);
		
		db.addChannel(name, channel);
		
		return "Channel created!";
	}

}
