package edu.upenn.cis.cis455.storage.classes;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class HashedDocument implements Serializable {

	private UUID docID;
	private byte[] hashedContent;
	
	public HashedDocument(UUID docID, String raw) {
		this.docID = docID;
		
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    md.update(raw.getBytes());
	    hashedContent = md.digest();
	}

	public byte[] getHashedContent() {
		return hashedContent;
	}

	public UUID getDocID() {
		return docID;
	}

	
}
