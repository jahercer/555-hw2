package edu.upenn.cis.cis455.storage.classes;

import java.io.Serializable;
import java.util.List;

public class DocMatches implements Serializable {

	List<Document> documents;
	
	public DocMatches() {
	}
	
	public List<Document> getDocuments() {
		return documents;
	}
	
	public void setDocuments(List<Document> docs) {
		documents = docs;
	}
	
	public void addMatch(Document doc) {
		documents.add(doc);
	}
}
