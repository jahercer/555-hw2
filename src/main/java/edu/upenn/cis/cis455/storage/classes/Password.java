package edu.upenn.cis.cis455.storage.classes;

import java.io.Serializable;

public class Password implements Serializable {

	private byte[] content;
	
	public Password(byte[] content) {
		this.content = content;
	}
	
	public byte[] getContent() {
		return this.content;
	}
	
	
}
