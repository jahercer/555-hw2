package edu.upenn.cis.cis455.storage.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DisallowValue implements Serializable {

	private List<String> paths;
	private int delay;
	
	public DisallowValue() {
		this.delay = -1;
		this.paths = new ArrayList<String>();
	}
	
	public DisallowValue(int delay) {
		this.delay = delay;
	}
	
	public List<String> getPaths() {
		return paths;
	}

	public int getDelay() {
		return delay;
	}
	
	public void setDelay(int delay) {
		this.delay = delay;
	}
	
}
