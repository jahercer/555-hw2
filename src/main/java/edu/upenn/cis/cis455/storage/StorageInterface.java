package edu.upenn.cis.cis455.storage;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.classes.Channel;
import edu.upenn.cis.cis455.storage.classes.DisallowValue;
import edu.upenn.cis.cis455.storage.classes.Document;
import edu.upenn.cis.cis455.storage.classes.WebPage;
import edu.upenn.cis.cis455.storage.classes.Password;

public interface StorageInterface {

    /**
     * How many documents so far?
     */
    public int getCorpusSize();

    /**
     * Add a new document, getting its ID
     */
    public UUID addDocument(String url, Document doc);
    
    /**
     * Add a new channel to db
     */
    public void addChannel(String name, Channel channel);
    
    /**
     * Add matched url to path
     */
    public void addMatchedUrl(String path, String url);
    
    /**
     * Get arrays of paths
     */
    public String[] getPaths();
    
    /**
     * Get arrays of channel names
     */
    public Set<String> getChannels();
    
    /**
     * Returns channel
     */
    public Channel getChannel(String name);
    
    /**
     * Adds URL when the document is already downloaded
     */
	public void addUrlOnly(String url, UUID docID);
	
	/**
    * Adds url and docID to content seen db
    */
	public void addContentSeen(String url, UUID docID);

    /**
     * Retrieves a document's contents by URL
     */
    public Document getDocument(String url);
    
    /**
     * Retrieves a HASHED document's contents by URL
     */
    public byte[] getHashedDocument(String url);
    
    /**
     * Checks if exists a document with the param HASHED content 
     * Returns docId or null 
     */
    public UUID findHashedDocument(byte[] content);
    
    /**
     * Checks if user is already registered
     */
	public boolean checkUser(String user);

    /**
     * Adds a user and returns an ID
     */
    public void addUser(String username, Password password);

    /**
     * Tries to log in the user, or else throws a HaltException
     */
    public boolean getSessionForUser(String username, Password password);
    
    /**
     * Returns the Robots.txt info from Database (null if not found)
     */
    public DisallowValue getDisallowInfo(URLInfo url);
    
    /**
     * Adds a disallow host to DisallowDb
     */
    public void addDisallowInfo(String hostName, DisallowValue dis);
    
    /**
     * Checks if the url has been visited during this crawl
     */
    public boolean checkContentSeen(String url);
    
    /**
     * Checks if the url has been visited before
     */
    public boolean checkURL(String url);
    
    /**
     * Returns WebPage value
     */
    public WebPage getURL(String url);
    
    /**
     * Checks last time we accessed a URL from the root url (robots.txt)
     */
    public Date getLastTimeRootUrl(String stringNoPath);

    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close();
    
    /**
     * Empty content-seen database
     */
    public void empty();


}
