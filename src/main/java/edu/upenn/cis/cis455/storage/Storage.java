package edu.upenn.cis.cis455.storage;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.sleepycat.je.Environment;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.classes.Channel;
import edu.upenn.cis.cis455.storage.classes.DisallowValue;
import edu.upenn.cis.cis455.storage.classes.Document;
import edu.upenn.cis.cis455.storage.classes.HashedDocument;
import edu.upenn.cis.cis455.storage.classes.Password;
import edu.upenn.cis.cis455.storage.classes.WebPage;

public class Storage implements StorageInterface {

	private Environment env;
	
	public Storage(Environment env) {
		this.env = env;
	}
	
	@Override
	public int getCorpusSize() {
		return StorageFactory.getDocumentsMap().entrySet().size();
	}

	@Override
	public UUID addDocument(String url, Document doc) {
		UUID docID = UUID.randomUUID();
		StorageFactory.getDocumentsMap().put(docID, doc);
		StorageFactory.getUrlMap().put(url, new WebPage(docID));
		return docID;
	}

	@Override
	public Document getDocument(String url) {
		
		Document doc = StorageFactory.getDocumentsMap().get(StorageFactory.getUrlMap().get(url).getDocID());
		return doc != null ? doc : null;
	}

	@Override
	public void addUser(String username, Password password) {
		StorageFactory.getUsersMap().put(username, password);
	}

	@Override
	public boolean getSessionForUser(String username, Password password) {
		Password pass = StorageFactory.getUsersMap().get(username);
		
		if (pass != null && Arrays.equals(password.getContent(), pass.getContent())) {
			return true;
		} else return false;
	}

	@Override
	public void close() {
		StorageFactory.getClassCatalog().close();
		StorageFactory.getUsersDb().close();
		StorageFactory.getUrlDb().close();
		StorageFactory.getDocumentsDb().close();
		empty();
		StorageFactory.getContentSeenDb().close();
		StorageFactory.getDisallowDb().close();
		env.close();
	}

	public Environment getEnv() {
		return env;
	}

	@Override
	public DisallowValue getDisallowInfo(URLInfo url) {
		return (DisallowValue) StorageFactory.getDisallowMap().get(url.getHostName());
	}

	@Override
	public void addDisallowInfo(String hostName, DisallowValue dis) {
		StorageFactory.getDisallowMap().put(hostName, dis);
	}

	@Override
	public boolean checkContentSeen(String url) {
		return StorageFactory.getContentSeenMap().keySet().contains(url);
	}

	@Override
	public boolean checkURL(String url) {
		return StorageFactory.getUrlMap().containsKey(url);
	}

	@Override
	public WebPage getURL(String url) {
		return (WebPage) StorageFactory.getUrlMap().get(url);
	}

	@Override
	public byte[] getHashedDocument(String url) {
		Document doc = (Document) StorageFactory.getDocumentsMap().get(StorageFactory.getContentSeenMap().get(url));
		if (doc != null) return doc.getHashedContent();
		else return null;
	}

	@Override
	public UUID findHashedDocument(byte[] content) {
		Set<UUID> keys = StorageFactory.getDocumentsMap().keySet();
		Iterator<UUID> it = keys.iterator();
		while(it.hasNext()) {
			UUID key = it.next();
			Document doc = (Document) StorageFactory.getDocumentsMap().get(key);
			if (Arrays.equals(content, doc.getHashedContent())) {
				return key;
			}
		}
		
		return null;
	}

	@Override
	public void addUrlOnly(String url, UUID docID) {
		StorageFactory.getUrlMap().put(url, new WebPage(docID));
	}

	@Override
	public void empty() {
		StorageFactory.getContentSeenMap().clear();
	}

	@Override
	public void addContentSeen(String url, UUID docID) {
		StorageFactory.getContentSeenMap().put(url, docID);
	}

	@Override
	public synchronized Date getLastTimeRootUrl(String hostName) {
		Date max = new Date(0L);

		for (Map.Entry<String, WebPage> entry :  StorageFactory.getUrlMap().entrySet()) {
			URLInfo urlInfo = new URLInfo(entry.getKey());
			if (urlInfo.getHostName().equals(hostName)) {
				Date accDate = entry.getValue().getDate();
				if (accDate.after(max)) {
					max = accDate;
				}
			}
		}
		return max;
	}

	@Override
	public boolean checkUser(String user) {
		return StorageFactory.getUsersMap().containsKey(user);
	}

	@Override
	public void addChannel(String name, Channel channel) {
		System.out.println("Adding channel: "+name);
		StorageFactory.getPathsMap().put(name, channel);
	}

	@Override
	public Channel getChannel(String name) {
		System.out.println("Retrieving channel: "+name);
		return StorageFactory.getPathsMap().get(name);
	}

	@Override
	public void addMatchedUrl(String path, String url) {
		for (String s : StorageFactory.getPathsMap().keySet()) {
			if (path.equals(StorageFactory.getPathsMap().get(s).getPath())) {
				System.out.println("Path found: Adding url!");
				Channel c = StorageFactory.getPathsMap().get(s);
				c.addUrl(url);
				StorageFactory.getPathsMap().put(s, c);
				break;
			}
		}
	}

	@Override
	public String[] getPaths() {
		String[] res = new String[StorageFactory.getPathsMap().size()];
		int i = 0;
 		for (String s : StorageFactory.getPathsMap().keySet()) {
			res[i] = StorageFactory.getPathsMap().get(s).getPath();
			i++;
		}
 		
 		return res;
	}

	@Override
	public Set<String> getChannels() {
		return StorageFactory.getPathsMap().keySet();
	}




}
