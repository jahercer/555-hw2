package edu.upenn.cis.cis455.storage.classes;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Document implements Serializable {

	private String contentType;
	private String rawContent;
	private byte[] hashedContent;
	
	public Document(String raw, String contentType) {
		this.contentType = contentType;
		setRawContent(raw);
		
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    md.update(raw.getBytes());
	    hashedContent = md.digest();
	}

	public String getRawContent() {
		return rawContent;
	}

	public void setRawContent(String rawContent) {
		this.rawContent = rawContent;
	}

	public byte[] getHashedContent() {
		return hashedContent;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
}
