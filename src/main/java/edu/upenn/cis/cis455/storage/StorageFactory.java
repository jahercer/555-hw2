package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import edu.upenn.cis.cis455.storage.classes.Channel;
import edu.upenn.cis.cis455.storage.classes.DisallowValue;
import edu.upenn.cis.cis455.storage.classes.DocMatches;
import edu.upenn.cis.cis455.storage.classes.Document;
import edu.upenn.cis.cis455.storage.classes.HashedDocument;
import edu.upenn.cis.cis455.storage.classes.Password;
import edu.upenn.cis.cis455.storage.classes.WebPage;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredEntrySet;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.UUID;

public class StorageFactory {
	
	private static Environment env;
	private static final String CLASS_CATALOG = "java_class_catalog";
	private static final String USERS_STORE = "users_store";
	private static final String URL_STORE = "url_store";
	private static final String DOCUMENTS_STORE = "documents_store";
	private static final String CONTENT_SEEN_STORE = "content_seen_store";
	private static final String DISALLOW_STORE = "disallow_store";
	private static final String PATHS_STORE = "paths_store";
	
	private static StoredClassCatalog javaCatalog;
	
	private static Database usersDb;
    private static Database urlDb;
    private static Database documentsDb;
    private static Database contentSeenDb;
    private static Database disallowDb;
    private static Database pathsDb;
    
    private static StoredSortedMap<String, Password> usersMap;
    private static StoredSortedMap<String, WebPage> urlMap;
    private static StoredSortedMap<UUID, Document> documentsMap;
    private static StoredSortedMap<String, UUID> contentSeenMap;
    private static StoredSortedMap<String, DisallowValue> disallowMap;
    private static StoredSortedMap<String, Channel> pathsMap;
	
    public static StorageInterface getDatabaseInstance(String directory) throws DatabaseException, FileNotFoundException {
        // TODO: factory object, instantiate your storage server
    	
    	EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);

        env = new Environment(new File(directory), envConfig);
        
        //Create Class Catalog
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        Database catalogDb = env.openDatabase(null, CLASS_CATALOG, dbConfig);
        javaCatalog = new StoredClassCatalog(catalogDb);
        
        //Initialize our DBs
        usersDb = env.openDatabase(null, USERS_STORE, dbConfig);
        urlDb = env.openDatabase(null, URL_STORE, dbConfig);
        documentsDb = env.openDatabase(null, DOCUMENTS_STORE, dbConfig);
        contentSeenDb = env.openDatabase(null, CONTENT_SEEN_STORE, dbConfig);
        disallowDb = env.openDatabase(null, DISALLOW_STORE, dbConfig);
        pathsDb = env.openDatabase(null, PATHS_STORE, dbConfig);
        
        //Create bindings
        EntryBinding userKeyBinding = new SerialBinding(javaCatalog, String.class);
        EntryBinding userDataBinding = new SerialBinding(javaCatalog, Password.class);
        EntryBinding urlKeyBinding = new SerialBinding(javaCatalog, String.class);
        EntryBinding urlDataBinding = new SerialBinding(javaCatalog, WebPage.class);
        EntryBinding documentsKeyBinding = new SerialBinding(javaCatalog, UUID.class);
        EntryBinding documentsDataBinding = new SerialBinding(javaCatalog, Document.class);
        EntryBinding contentSeenKeyBinding = new SerialBinding(javaCatalog, String.class);
        EntryBinding contentSeenDataBinding = new SerialBinding(javaCatalog, UUID.class);
        EntryBinding disallowKeyBinding = new SerialBinding(javaCatalog, String.class);
        EntryBinding disallowDataBinding = new SerialBinding(javaCatalog, DisallowValue.class);
        EntryBinding channelKeyBinding = new SerialBinding(javaCatalog, String.class);
        EntryBinding channelDataBinding = new SerialBinding(javaCatalog, Channel.class);
        
        //Initialize Maps
        usersMap = new StoredSortedMap(usersDb, userKeyBinding, userDataBinding, true);
        urlMap = new StoredSortedMap(urlDb, urlKeyBinding, urlDataBinding, true);
        documentsMap = new StoredSortedMap(documentsDb, documentsKeyBinding, documentsDataBinding, true);
        contentSeenMap = new StoredSortedMap(contentSeenDb, contentSeenKeyBinding, contentSeenDataBinding, true);
        disallowMap = new StoredSortedMap(disallowDb, disallowKeyBinding, disallowDataBinding, true);
        pathsMap = new StoredSortedMap(pathsDb, channelKeyBinding, channelDataBinding, true);
    	
        return new Storage(env);
    }

	public static final StoredClassCatalog getClassCatalog() {
		return javaCatalog;
	}

	public static Database getUsersDb() {
		return usersDb;
	}

	public static final Database getUrlDb() {
		return urlDb;
	}
	
	public static final Database getDocumentsDb() {
		return documentsDb;
	}

	public static final Database getContentSeenDb() {
		return contentSeenDb;
	}

	public static final Database getDisallowDb() {
		return disallowDb;
	}
	
	public static final Database getPathsDb() {
		return pathsDb;
	}

	public static final StoredSortedMap<String, Password> getUsersMap() {
		return usersMap;
	}

	public static final StoredSortedMap<String, WebPage> getUrlMap() {
		return urlMap;
	}
	
	public static final StoredSortedMap<UUID, Document> getDocumentsMap() {
		return documentsMap;
	}

	public static final StoredSortedMap<String, UUID> getContentSeenMap() {
		return contentSeenMap;
	}

	public static final StoredSortedMap<String, DisallowValue> getDisallowMap() {
		return disallowMap;
	}
	
	public static final StoredSortedMap<String, Channel> getPathsMap() {
		return pathsMap;
	}
	
	public final StoredEntrySet getUsersEntrySet() {
        return (StoredEntrySet) usersMap.entrySet();
    }
	
	public final StoredEntrySet getUrlEntrySet() {
        return (StoredEntrySet) urlMap.entrySet();
    }
	
	public final StoredEntrySet getDocumentsEntrySet() {
        return (StoredEntrySet) documentsMap.entrySet();
    }
	
	public final StoredEntrySet getContentSeenEntrySet() {
        return (StoredEntrySet) contentSeenMap.entrySet();
    }
	
	public final StoredEntrySet getDisallowEntrySet() {
        return (StoredEntrySet) disallowMap.entrySet();
    }
	
	public final StoredEntrySet getPathsEntrySet() {
        return (StoredEntrySet) pathsMap.entrySet();
    }
	
}
