package edu.upenn.cis.cis455.storage.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Channel implements Serializable {

	private String username;
	private String path;
	private List<String> urls = new ArrayList<String>();
	
	public Channel(String username, String path) {
		this.username = username;
		this.path = path;
	}
	
	public void addUrl(String url) {
		urls.add(url);
	}

	public String getUsername() {
		return username;
	}

	public String getPath() {
		return path;
	}
	
	public List<String> getUrls() {
		return urls;
	}
	
}
