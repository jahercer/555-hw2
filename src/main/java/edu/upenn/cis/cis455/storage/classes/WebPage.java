package edu.upenn.cis.cis455.storage.classes;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class WebPage implements Serializable {

	private UUID docID;
	private Date date;
	
	public WebPage(UUID docID) {
		this.docID = docID;
		this.date = new Date();
	}
	
	public UUID getDocID() {
		return docID;
	}

	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
}
